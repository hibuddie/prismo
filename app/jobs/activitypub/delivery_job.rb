# frozen_string_literal: true

class ActivityPub::DeliveryJob < ApplicationJob
  queue_as :push

  HEADERS = { 'Content-Type' => 'application/activity+json' }.freeze

  def perform(json, source_account_id, inbox_url, options = {})
    @json           = json
    @source_account = ::Account.find(source_account_id)
    @inbox_url      = inbox_url
    @options        = options.with_indifferent_access

    perform_request
  rescue => e
    raise e.class, "Delivery failed for #{inbox_url}: #{e.message}", e.backtrace[0]
  end

  private

  def build_request
    request = Request.new(:post, @inbox_url, body: @json)
    request.on_behalf_of(@source_account, :uri)
    request.add_headers(HEADERS)
  end

  def perform_request
    build_request.perform do |response|
      raise Prismo::Exceptions::UnexpectedResponseError, response unless response_successful?(response) || response_error_unsalvageable?(response)
    end
  end

  def response_successful?(response)
    (200...300).cover?(response.code)
  end

  def response_error_unsalvageable?(response)
    (400...500).cover?(response.code) && response.code != 429
  end
end
