# frozen_string_literal: true

module Accounts
  class SilenceJob < ApplicationJob
    queue_as :default

    def perform(account_id)
      SilenceAccountService.new.call(
        Account.find(account_id)
      )
    end
  end
end
