# frozen_string_literal: true

class StaticController < ApplicationController
  layout 'application'

  def design; end
end
