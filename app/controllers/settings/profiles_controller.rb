# frozen_string_literal: true

class Settings::ProfilesController < ApplicationController
  before_action :authenticate_user!

  layout 'settings'

  def show
    @account = current_user.account
    @destroy = Accounts::Delete.new
  end

  def update
    @account = current_user.account

    if @account.update_attributes(account_params)
      ActivityPub::UpdateDistributionJob.call_later(@account)
      redirect_to settings_profile_path, notice: I18n.t('generic.changes_saved_msg')
    else
      render :show
    end
  end

  def destroy
    outcome = Accounts::Delete.run(
      account: current_user.account,
      current_password: params[:accounts_delete][:current_password]
    )

    if outcome.valid?
      sign_out current_user
      flash[:success] = t('accounts.destroy.success')
      redirect_to :root
    else
      flash[:alert] = outcome.errors.full_messages.to_sentence
      redirect_to settings_profile_path
    end
  end

  private

  def account_params
    params.require(:account).permit(:display_name, :bio, :avatar, :remove_avatar, :theme)
  end
end
