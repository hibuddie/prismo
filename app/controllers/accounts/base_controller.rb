# frozen_string_literal: true

module Accounts
  class BaseController < ApplicationController
    before_action { set_jumpbox_link(find_account) }

    private

    def find_account
      Account.local.find_by!(username: params[:username]).decorate
    end
  end
end
