# frozen_string_literal: true

class StoriesController < ApplicationController
  layout 'application'

  before_action :authenticate_user!, only: %i[new create edit update]
  before_action :set_account_liked_story_ids

  def index
    @page_title = 'Hot stories'
    @feed_title = @page_title

    stories = StoriesQuery.new.hot
    stories = StoriesQuery.new(stories).without_silenced

    @stories = stories.page(params[:page])

    respond_to do |format|
      format.html { render :index }
      format.atom { render :index }
    end
  end

  def recent
    @page_title = 'Recent stories'
    @feed_title = @page_title

    stories = StoriesQuery.new.recent
    stories = StoriesQuery.new(stories).without_silenced

    @stories = stories.page(params[:page])

    respond_to do |format|
      format.html { render :index }
      format.atom { render :index }
    end
  end

  def show
    set_account_liked_comment_ids

    @story = ActivityPubPost.find(params[:id])
    @comments = @story.children.includes(:account, :root_cached).hash_tree
    @comment = Comments::Create.new(
      parent_id: @story.id
    )

    respond_to do |format|
      format.html
      format.json do
        render json: ActivityPub::StorySerializer.new(@story),
               with_context: true,
               content_type: 'application/activity+json'
      end
    end
  end

  def new
    @story = Stories::Create.new(*new_story_params)
    authorize @story
  end

  def create
    authorize Stories::Create

    outcome = Stories::Create.run(
      name: params.fetch(:story)[:name],
      url: params.fetch(:story)[:url],
      content_source: params.fetch(:story)[:content_source],
      tag_list: params.fetch(:story)[:tag_list],
      account: current_account
    )

    if outcome.valid?
      path = outcome.result.decorate.path
      redirect_to path, notice: 'Story has been created'
    else
      @story = outcome
      render :new
    end
  end

  def edit
    story = find_story
    authorize story

    @story = Stories::Update.new(
      story: story,
      url: story.url,
      name: story.name,
      tag_list: story.tag_names.join(', '),
      content_source: story.content_source,
    )
  end

  def update
    story = find_story
    authorize story

    outcome = Stories::Update.run(
      story: story,
      url: params.fetch(:story)[:url],
      name: params.fetch(:story)[:name],
      tag_list: params.fetch(:story)[:tag_list],
      content_source: params.fetch(:story)[:content_source],
      account: current_account
    )

    if outcome.valid?
      path = outcome.result.decorate.path
      redirect_to path, notice: 'Story has been updated'
    else
      @story = outcome
      render :edit
    end
  end

  private

  def new_story_params
    params.permit(:url, :title)
  end

  def find_story
    ActivityPubPost.find(params[:id])
  end
end
