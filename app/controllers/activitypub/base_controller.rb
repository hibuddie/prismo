# frozen_string_literal: true

class ActivityPub::BaseController < ActionController::Base
  skip_before_action :verify_authenticity_token
end
