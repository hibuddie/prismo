# frozen_string_literal: true

module Settings
  class UpdatePreferencesForm < ActiveInteraction::Base
    THEMES = %w[default dark].freeze

    object :user
    string :theme

    validates :theme, presence: true
    validates :theme, allow_blank: true, inclusion: { in: THEMES }

    def initialize(args)
      self.theme ||= args[:user].settings.theme || THEMES.first

      super
    end

    def execute
      user.settings.theme = theme if theme?
    end
  end
end
