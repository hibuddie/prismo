# frozen_string_literal: true

class Stories::BroadcastChanges < ActiveInteraction::Base
  object :story, class: ActivityPubPost

  def execute
    ActionCable.server.broadcast 'updates_channel', {
      event: 'stories.updated',
      data: ActivityPub::StorySerializer.new(story).data
    }
  end
end
