# frozen_string_literal: true

class Stories::ToggleLike < ActiveInteraction::Base
  object :story, class: ActivityPubPost
  object :account

  def execute
    if account.liked?(story)
      compose(Stories::Unlike, inputs)
    else
      compose(Stories::Like, inputs)
    end
  end
end
