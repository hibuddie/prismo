# frozen_string_literal: true

class Comments::Unlike < ActiveInteraction::Base
  object :comment, class: ActivityPubComment
  object :account

  def execute
    like = ::Like.find_by(account: account, likeable: comment)

    if like.present?
      like.destroy
      Accounts::UpdateKarmaJob.perform_later(comment.account.id, 'Comment', 'remove')
      ActivityPub::UndoLikeDistributionJob.call(like) if !comment.account.local?
    end
    # Stories::BroadcastChanges.run! story: story.reload

    like
  end
end
