# frozen_string_literal: true

module Gutentag
  class TagPolicy < ApplicationPolicy
    def toggle_follow?
      user.present?
    end
  end
end
