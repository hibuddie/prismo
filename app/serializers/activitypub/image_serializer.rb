# frozen_string_literal: true

class ActivityPub::ImageSerializer < ActivityPub::BaseSerializer
  def data
    {
      type: 'Image',
      url: image_url(object[:size_400].url, host: ActionController::Base.asset_host),
      media_type: object[object.keys.first].mime_type
    }
  end
end
