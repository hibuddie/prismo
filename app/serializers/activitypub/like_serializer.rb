# frozen_string_literal: true

class ActivityPub::LikeSerializer < ActivityPub::BaseSerializer
  def data
    {
      id: id,
      object: parent_object,
      type: 'Like',
      actor: ActivityPub::TagManager.instance.uri_for(object.account)
    }
  end

  def id
    [ActivityPub::TagManager.instance.uri_for(object.account), '#likes/', object.id].join
  end

  private

  def parent_object
    case object
    when ActivityPubPost then ActivityPub::StorySerializer.new(object).data
    when ActivityPubComment then ActivityPub::CommentSerializer.new(object).data
    end
  end
end
