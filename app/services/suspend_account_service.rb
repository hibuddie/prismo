# frozen_string_literal: true

class SuspendAccountService
  # Suspend an account and remove as much of its data as possible
  #
  # @param [Account]
  # @param [Hash] options
  # @option [Boolean] :including_user Remove the user record as well
  # @option [Boolean] :destroy Remove the account record instead of suspending
  def call(account, **options)
    @account = account
    @options = options

    purge_user!
    purge_profile!
    purge_content!
    distribute_delete_actor! if account.local?
  end

  private

  attr_reader :account, :options

  def purge_user!
    return if !account.local? || account.user.nil?

    if options[:including_user]
      account.user.destroy
    else
      account.user.disable!
    end
  end

  def purge_profile!
    # If the account is going to be destroyed
    # there is no point wasting time updating
    # its values first
    return if options[:destroy]

    account.silenced         = false
    account.suspended        = true
    account.locked           = false
    account.display_name     = ''
    account.bio              = ''
    account.stories_count    = 0
    account.comments_count   = 0
    account.followers_count  = 0
    account.following_count  = 0
    # account.moved_to_account = nil
    account.comments_karma   = 0
    account.posts_karma      = 0
    account.avatar           = nil
    account.save!
  end

  def purge_content!
    account.comments.each { |c| Comments::Delete.run!(comment: c) }
    account.stories.each  { |s| Stories::Delete.run!(story: s) }
    account.likes.destroy_all
    account.notifications_as_recipient.destroy_all
    account.follow_requests.destroy_all
    account.active_follows.destroy_all
    account.passive_follows.destroy_all

    account.destroy if options[:destroy]
  end

  def distribute_delete_actor!
    delivery_inboxes.each do |inbox_url|
      ActivityPub::DeliveryJob.perform_later(
        signed_payload, account.id, inbox_url
      )
    end
  end

  def signed_payload
    @signed_payload ||= begin
      Oj.dump(
        ActivityPub::LinkedDataSignature.new(payload).sign!(account)
      )
    end
  end

  def payload
    ActivityPub::DeleteActorSerializer.new(account, with_context: true).as_json
  end

  def delivery_inboxes
    Account.inboxes.reject(&:empty?)
  end
end
