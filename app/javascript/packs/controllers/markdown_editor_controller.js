import BaseController from "./base_controller"
import autosize from 'autosize'
import api from '../lib/api'

export default class extends BaseController {
  static targets = ['input', 'editor', 'preview']

  initialize () {
    autosize(this.inputTarget)
  }

  togglePreview (e) {
    e.preventDefault()

    this.isPreviewing = !this.isPreviewing
  }

  showEditor () {
    this.previewTarget.classList.remove('active')
    this.editorTarget.classList.add('active')
  }

  showPreview () {
    this.editorTarget.classList.remove('active')
    this.previewTarget.classList.add('active')

    this._refreshPreview()
  }

  _refreshPreview () {
    let req = api.markdownPreview(this.inputTarget.value)

    req.then((resp) => {
      this.preview = resp.data
    })
  }

  set preview (value) {
    this.previewTarget.innerHTML = value
  }

  set isPreviewing (value) {
    this.data.set('isPreviewing', value)

    if (this.isPreviewing) {
      this.showPreview()
    } else {
      this.showEditor()
    }
  }

  get isPreviewing () {
    return this.data.get('isPreviewing') == 'true'
  }
}
