class AddSilencedToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :silenced, :boolean, default: false
  end
end
