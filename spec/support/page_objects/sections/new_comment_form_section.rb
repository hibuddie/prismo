# frozen_string_literal: true

class NewCommentFormSection < SitePrism::Section
  element :body_input, 'form#new_comment [name="comment[body]"]'
  element :submit_btn, 'input[type="submit"]'

  def add_comment(body: nil)
    body_input.set body
    submit_btn.click
  end
end
