# frozen_string_literal: true

require_relative '../../sections/comment_section'

module Accounts
  class CommentsPage < SitePrism::Page
    set_url '/@{username}/comments'
    set_url_matcher %r{\/@\w+\/comments\z}

    sections :comments, CommentSection, '.comment'
  end
end
