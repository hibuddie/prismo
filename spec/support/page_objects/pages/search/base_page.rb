class Search::BasePage < SitePrism::Page
  element :query_field, '#q'
  element :submit_button, 'button[type="submit"]'

  def search(query)
    query_field.set(query)
    submit_button.click
  end
end
