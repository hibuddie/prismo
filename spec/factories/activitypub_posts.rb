# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :activitypub_post do
    account
    tag_names %w[foo bar]
    uuid { SecureRandom.uuid }
    group

    trait :link do
      sequence(:url) { Faker::Internet.url }
      sequence(:name) { Faker::Company.catch_phrase }
    end

    trait :text do
      url nil
      sequence(:content_source) { 'Sample content' }
      sequence(:content) { '<p>Sample content</p>' }
    end

    trait :not_local do
      uri Faker::Internet.device_token
    end

    trait :removed do
      removed { true }
    end
  end
end
