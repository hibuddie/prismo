# frozen_string_literal: true

require 'rails_helper'

describe Accounts::StoriesController, type: :controller do
  render_views

  let(:account) { create(:account) }

  describe 'GET #hot' do
    subject { get :hot, params: { username: account.username }, format: format }

    context 'when requesting html' do
      let(:format) { :html }

      it 'renders successfull response' do
        subject
        expect(response).to be_successful
        expect(response.content_type).to eq 'text/html'
      end
    end

    context 'when requesting atom' do
      let(:format) { :atom }

      it 'renders successfull response' do
        subject
        expect(response).to be_successful
        expect(response.content_type).to eq 'application/atom+xml'
      end
    end

    context 'when requesting json' do
      let(:format) { :json }

      it 'renders successfull response' do
        subject
        expect(response).to be_successful
        expect(response.content_type).to eq 'application/activity+json'
      end
    end
  end

  describe 'GET #recent' do
    subject { get :recent, params: { username: account.username } }

    it 'renders successfull response' do
      subject
      expect(response).to be_successful
      expect(response.content_type).to eq 'text/html'
    end
  end
end
